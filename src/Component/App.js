import React, {Component} from 'react';
import Searchbar from './Searchbar';
import youtube from '../apis/youtube';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';
const KEY = 'AIzaSyApdhK1KI9BfvOoBoAJlsb5c1M0VmhNqbQ';



class App extends Component {
    state = {
        videos: [],
        selectedVideo: null
    }
    handleSubmit = async (termFromSearchBar) => {
        const response = await youtube.get('/search', {
            params: {
                q: termFromSearchBar,
                part: 'snippet',
                maxResults: 5,
                key: KEY     
            }
        })
        this.setState({
            videos: response.data.items
        })
    };

    handleVideoSelect = (video) => {
        this.setState({selectedVideo: video})
    }

    render() {
        return (
            <div className="ui container" style={{marginTop:'1em'}}>
                <Searchbar handleFormSubmit={this.handleSubmit}/>
                <div className="ui grid">
                    <div className="ui row">
                        <div className="eleven wide column">
                            <VideoDetail 
                            video={this.state.selectedVideo}/>
                        </div>
                        <div className="five wide column">
                            <VideoList 
                            handleVideoSelect={this.handleVideoSelect} 
                            videos={this.state.videos}/>         
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default App;
